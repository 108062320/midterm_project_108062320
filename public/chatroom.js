// global variable
var user_name = ''; // email
var account = ''; // string before email's @
var Ref_my_chatroom_list;
var current_chatroom = 'null'; 
var postsRef = firebase.database().ref('protected/null');
// avoid innerHTML contains HTML code
function html_encode(str) {
    var s = "";
    if (str == "") return "";
    s = str.replace(/</g, "&lt;");
    s = s.replace(/>/g, "&gt;");
    return s;
}
// pop out the form
function buttonAdd() {
    document.getElementById("myForm").style.display = "block";
    document.getElementById('chatroom_list').scrollTop = document.getElementById('chatroom_list').scrollHeight;
}
// close the form
function buttonClose() {
    document.getElementById("myForm").style.display = "none";
    document.getElementById('chatroom_list').scrollTop = document.getElementById('chatroom_list').scrollHeight;
}
// create a new chatroom
function buttonCreate() {
    if (account != '') {
        var firebase_ref_my_chatroom_list = firebase.database().ref('protected/' + account + '/my_chatroom_list');
        var chatroom_name = document.getElementById("new_chatroom_name").value;
        firebase_ref_my_chatroom_list.push(chatroom_name);
    }
    else {
        alert("You have to login first.");
    }
    chatroom_name.value = '';
    close_btn.click();
}
// get cahtroom name
function get_chatroom_name(chatroom_name) {
    current_chatroom = 'chatroom_name_' + chatroom_name;
    postsRef = firebase.database().ref('protected/' + current_chatroom);
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    var chatroom_top = document.getElementById('chatroom_top');
    chatroom_top.innerHTML = '<img src="https://static-cdn.jtvnw.net/jtv_user_pictures/asiagodtonegg3be0-profile_image-081feb3428b1a1c6-300x300.jpeg" alt=""><div><h2>' + chatroom_name + '</h2><h3>already ' + second_count + ' messages</h3><button onclick="buttonOpen_add_member(\'' + chatroom_name + '\')">add member</button></div>' + '<div class="form-popup" id="add_member_form_' + chatroom_name + '"><form action="/action_page.php" class="form-container"><h1>Add a new member.</h1><label for="psw"><b>New member name:</b></label><input type="text" id="new_member_name_' + chatroom_name + '" required><button type="button" class="btn_create" id="add_btn_add_member_' + chatroom_name + '" onclick="buttonAdd_add_member(\'' + chatroom_name + '\')">Add</button><button type="button" class="btn_cancel" id="close_btn_add_member_' + chatroom_name + '" onclick="buttonClose_add_member(\'' + chatroom_name + '\')">Close</button></form></div>';


    postsRef.once('value')
        .then(function (snapshot) {
            if (user_name != '') {
                snapshot.forEach(function (childshot) {
                    var childData = childshot.val();
                    var post_string = '';
                    if (user_name == childData.name) {
                        post_string = '<li class="' + 'me' + '"><div class="entete"><h3>' + childData.date + '</h3><br><h2>' + childData.username + '</h2><span class="status blue"></span></div><div class="triangle"></div><div class="message">' + childData.text + '</div></li>';
                    }
                    else {
                        post_string = '<li class="' + 'you' + '"><div class="entete"><h3>' + childData.date + '</h3><br><h2>' + childData.username + '</h2><span class="status blue"></span></div><div class="triangle"></div><div class="message">' + childData.text + '</div></li>';
                    }
                    // let the scroll keep at the bottom
                    document.getElementById('chat').scrollTop = document.getElementById('chat').scrollHeight;
                    total_post[total_post.length] = post_string;
                    first_count += 1;

                });
            }

            document.getElementById('chat').innerHTML = total_post.join('');

            postsRef.on('child_added', function (data) {
                if (user_name != '') {
                    second_count += 1;
                    var post_string = '';
                    if (second_count > first_count) {
                        var childData = data.val();

                        if (user_name == childData.name) {
                            post_string = '<li class="' + 'me' + '"><div class="entete"><h3>' + childData.date + '</h3><br><h2>' + childData.username + '</h2><span class="status blue"></span></div><div class="triangle"></div><div class="message">' + childData.text + '</div></li>';
                        }
                        else {
                            post_string = '<li class="' + 'you' + '"><div class="entete"><h3>' + childData.date + '</h3><br><h2>' + childData.username + '</h2><span class="status blue"></span></div><div class="triangle"></div><div class="message">' + childData.text + '</div></li>';
                            showNotification(account, chatroom_name);
                        }
                        total_post[total_post.length] = post_string;
                        document.getElementById('chat').innerHTML = total_post.join('');
                        
                    }
                    // let the scroll keep at the bottom
                    document.getElementById('chat').scrollTop = document.getElementById('chat').scrollHeight;

                    chatroom_top.innerHTML = '<img src="https://static-cdn.jtvnw.net/jtv_user_pictures/asiagodtonegg3be0-profile_image-081feb3428b1a1c6-300x300.jpeg" alt=""><div><h2>' + chatroom_name + '</h2><h3>already ' + second_count + ' messages</h3><button onclick="buttonOpen_add_member(\'' + chatroom_name + '\')">add member</button></div>' + '<div class="form-popup" id="add_member_form_' + chatroom_name + '"><form action="/action_page.php" class="form-container"><h1>Add a new member.</h1><label for="psw"><b>New member name:</b></label><input type="text" id="new_member_name_' + chatroom_name + '" required><button type="button" class="btn_create" id="add_btn_add_member_' + chatroom_name + '" onclick="buttonAdd_add_member(\'' + chatroom_name + '\')">Add</button><button type="button" class="btn_cancel" id="close_btn_add_member_' + chatroom_name + '" onclick="buttonClose_add_member(\'' + chatroom_name + '\')">Close</button></form></div>';
                }
            });
        })
        .catch(e => console.log(e.message));

}
// open add meber form
function buttonOpen_add_member(chatroom_name) {
    document.getElementById("add_member_form_" + chatroom_name).style.display = "block";
}
// add member
function buttonAdd_add_member(chatroom_name) {
    if (account != '') {
        var temp_new_member_name = document.getElementById('new_member_name_' + chatroom_name).value
        var new_member_name = html_encode(temp_new_member_name);
        console.log(new_member_name);
        var user_ref = firebase.database().ref('protected/account_list/' + new_member_name);
        var userExist = false;
        user_ref.once('value').then(function (snapshot) {
            console.log(snapshot.val());
            userExist = (snapshot.val() == 'true') ? true : false;
            if (userExist) {
                var firbase_new_member_chatroom_list = firebase.database().ref('protected/' + new_member_name + '/my_chatroom_list');
                firbase_new_member_chatroom_list.push(chatroom_name);
                document.getElementById('new_member_name_' + chatroom_name).value = '';
                buttonClose_add_member(chatroom_name);
            }
            else {
                alert('User do not exist!');
                document.getElementById('new_member_name_' + chatroom_name).value = '';
            }
        })
    }

}
// close add member form
function buttonClose_add_member(chatroom_name) {
    document.getElementById("add_member_form_" + chatroom_name).style.display = "none";
}

function showNotification(new_message_account, new_message_chatroom_name) {
    console.log("FFFFF");
    const notification = new Notification("Hi, " + new_message_account, {
       body: "You have message from " + new_message_chatroom_name,
       icon: "img/guodong.jpg"
    })
}

function init() {
    console.log(Notification.permission);
    if (Notification.permission !== "denied") {
        Notification.requestPermission().then(permission => {
            console.log(permission);
        });
    }
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        console.log(user);
        if (user) {
            user_name = user.email;
            // chatroom
            console.log(user_name);
            var temp = user_name.split('@');
            account = temp[0];
            Ref_my_chatroom_list = firebase.database().ref('protected/' + account + '/my_chatroom_list');

            // nav bar
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var logoutbtn = document.getElementById('logout-btn');
            logoutbtn.addEventListener("click", function () {
                firebase.auth().signOut().then(function () {
                    alert("User sign out success!");
                }).catch(function (error) {
                    alert("User sign out failed!");
                })
            }, false);

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            user_name = '';
            account = '';
        }
        document.getElementById('chat').innerHTML = "";
    });

    // <div><h2>Prénom Nom</h2><h3><span class="status orange"></span>offline</h3></div>

    // List for store posts html
    var chatroom_total_post = [];
    // Counter for checking history post update complete
    var chatroom_first_count = 0;
    // Counter for checking when to update new post
    var chatroom_second_count = 0;

    Ref_my_chatroom_list = firebase.database().ref('protected/' + account + '/my_chatroom_list');
    Ref_my_chatroom_list.once('value').then(function (snapshot) {
        var post_string = '';
        var last_item = document.getElementById('chatroom_list').innerHTML;

        if (account != '') {
            snapshot.forEach(function (childshot) {
                post_string = '<li onclick="get_chatroom_name(\'' + childshot.val() + '\')"><img src="https://static-cdn.jtvnw.net/jtv_user_pictures/asiagodtonegg3be0-profile_image-081feb3428b1a1c6-300x300.jpeg" alt=""><div><h2>' + childshot.val() + '</h2><h3><span class="status orange"></span>offline</h3></div></li>';
                chatroom_total_post[chatroom_total_post.length] = post_string;
                chatroom_first_count += 1;
            })
            var last_item = document.getElementById('chatroom_list').innerHTML;
            chatroom_total_post[chatroom_total_post.length] = last_item;
            document.getElementById('chatroom_list').innerHTML = chatroom_total_post.join('');

            Ref_my_chatroom_list.on('child_added', function (childshot) {
                if (account != '') {
                    chatroom_second_count += 1;
                    var post_string = '';
                    if (chatroom_second_count > chatroom_first_count) {
                        post_string = '<li onclick="get_chatroom_name(\'' + childshot.val() + '\')"><img src="https://static-cdn.jtvnw.net/jtv_user_pictures/asiagodtonegg3be0-profile_image-081feb3428b1a1c6-300x300.jpeg" alt=""><div><h2>' + childshot.val() + '</h2><h3><span class="status orange"></span>offline</h3></div></li>';
                        last_item = chatroom_total_post[chatroom_total_post.length - 1];
                        chatroom_total_post[chatroom_total_post.length - 1] = post_string;
                        chatroom_total_post[chatroom_total_post.length] = last_item;
                        document.getElementById('chatroom_list').innerHTML = chatroom_total_post.join('');
                    }
                    // let the scroll keep at the bottom
                    document.getElementById('chat').scrollTop = 0;
                }

            });
        }
    }).catch(function (error) {
        console.log(error);
    });


    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "" && user_name != '' && current_chatroom != 'null') {
            var cur_date = new Date();
            cur_date = cur_date.toString();
            cur_date = cur_date.split("G");
            cur_date = cur_date[0];
            var avoid_innerHTML_text = html_encode(post_txt.value);
            console.log(post_txt.value);
            console.log(avoid_innerHTML_text);
            var data = {
                text: avoid_innerHTML_text,
                name: user_name,
                username: account,
                date: cur_date
            };
            postsRef.push(data);
        }
        else if (user_name == '') {
            alert('Please login first.');
        }
        else if (current_chatroom == 'null') {
            alert('Please select a chatroom first.');
        }
        post_txt.value = "";
    });

}

window.onload = function () {
    init();
};