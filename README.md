﻿# Software Studio 2021 Spring Midterm Project

## Topic
* Project Name : Midterm_Project_108062320

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# 作品網址：https://midterm-project-57bd2.web.app

## Website Detail Description
進入Chatroom Page後，初次使用者必須先從網頁頂部的navigation bar點選Login進入Login Page，接著使用者可以創建新帳號在登入或是直接使用google帳戶登入。
登入後網頁的畫面會回到起初的Chatroom Page，原本的navigation bar中會多出一個使用者帳戶的資訊，此時使用者可以在自己已經擁有的聊天室中開始與其他成員對話、接收來自其他成員的訊息，也可以自行創立聊天室、邀請其他成員進入，但邀請其他成員時必須知道對方的帳戶名稱，帳戶名稱為登入時的電子郵件帳戶@前的文字（例如：某用戶以123@gmail.com登入，他的帳戶名稱為123）。
另外，建議使用者打開網頁的通知，當別人傳訊息給自己時可以立即知道新訊息來自哪個聊天室。
若想切換帳號時，從navigation bar點選Logout即可登出，此時網頁依然會停留在Chatroom Page，但navigation bar已經看不到使用者的帳戶名稱了，而且聊天室的對話內容與聊天室對話數量顯示都將無法顯示，只會留下聊天室列表中的聊天室名稱，這是對使用者的一種保護，也是讓使用者確認自己已經登出的方式之一。
如果需要切換帳戶或是重新登入，則須再次點選Login，重複以上步驟。


# Components Description : 
1. Membership Mechanism：
具備sign up、sign in、google account等功能，在登入頁面分別以3個button呈現，並且將資料儲存於firebase的realtime database中。

2. Host on your Firebase page：
網頁由firebase deploy and host。

3. Database read/write：
若將database root的read、write直接設為true，使用者在網頁的console處可以直接竄改database中的資料，因此我將資料全部儲存於一個名為protected的node之下，如此一來可以提高資料庫的安全性。
![](https://i.imgur.com/l1LF2nS.png)

4. RWD：
我的網頁可以透過橫向與縱向的scroll bar正常運作。

5. Topic Key Functions：
可以在聊天室旁的房間列表創建私人房間、在房間中邀請其他用戶，並且依據不同的房間載入不同的對話紀錄、傳送訊息。

6. Sign Up/In with Google or other third-party accounts：
可以使用google帳戶登錄。

7. Add Chrome notification：
此通知可以提醒使用者有新訊息，並且，。
![](https://i.imgur.com/ZKvlgq6.png)
（"987654"是使用者帳戶的名字，"test"是聊天室名稱）

8. Use CSS animation：
在登入畫面剛載入時，sign in、sign up、google account等button會從畫面右側滑到畫面左側。

9. Deal with messages when sending html code：
將'<'與'>'替換成外觀上一樣、但本質不同於html code中tag的符號。

# Other Functions Description : 
1. 聊天室視窗中能夠紀錄目前共有幾則訊息 :
![](https://i.imgur.com/YiyjBOT.png)

2. 聊天室中的對話方格提供更多資訊 : 
包含訊息送出的時間、用戶名。
![](https://i.imgur.com/ZaLP8nw.png)

3. scroll bar : 
聊天室對話區中的scroll bar會在訊息送出後自動置底。
聊天室列表中的「創建聊天室」按鈕會打開一個往下展開的form，打開時scroll bar也會自動置底，使得操作更方便。

